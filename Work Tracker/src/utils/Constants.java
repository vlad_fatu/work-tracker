package utils;

import java.text.SimpleDateFormat;

public class Constants {
	
	public static SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm dd-MM-yyyy");
	public static SimpleDateFormat dateFormatterHHMM = new SimpleDateFormat("HH:mm");
	
	public static String JOB_ID_PREF = "com.android.tracker.JOB_ID_PREF";

}
